package com.softtek.academy.spring.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.softtek.academy.spring.beans.Person;

@Transactional
@Repository("personRepository")
public class PersonRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<Person> getAll(){
		return (List<Person>)entityManager
				.createQuery("SELECT p FROM Person p", Person.class).getResultList();
	}
	
	public void update(Person std) {
		Person person = entityManager.find(Person.class, std.getId());
		person.setAge(std.getAge());
		person.setName(std.getName());
	}

	public void delete(int id) {
		Person person = entityManager.find(Person.class, id);
		entityManager.remove(person);
	}

	public Person getPersonById(int id) {
		Person person = entityManager.find(Person.class, id);
		return person;
	}

	public void savePerson(Person person) {
		entityManager.persist(person);
		
	}

}
