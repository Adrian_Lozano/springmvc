package com.softtek.academy.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.dao.PersonDao;
import com.softtek.academy.spring.dao.PersonRepository;
import com.softtek.academy.spring.service.PersonService;

@Controller
public class PersonController {
	@Autowired
	PersonService personService;
	
	@RequestMapping(value = "/person", method = RequestMethod.GET)
	public ModelAndView student() {
		return new ModelAndView("person", "command", new Person());
	}

	@RequestMapping(value = "/addPerson", method = RequestMethod.POST)
	public String addUser(@ModelAttribute("user") Person person, ModelMap model) {
        model.addAttribute("name", person.getName());
        model.addAttribute("age", person.getAge());
        model.addAttribute("id", person.getId());
        personService.savePerson(person);
		return "redirect:/viewstd";
	}
	
	@RequestMapping("/viewstd")
	public String viewstudents(Model m) {
		List <Person> list = personService.getAll();
		m.addAttribute("list",list);
		return "viewstd";
	}
    @RequestMapping(value="/editstd/{id}")
    public String edit(@PathVariable int id, Model m){
        Person person = personService.getById(id);
        m.addAttribute("command", person);
        return "stdeditform";
    }
    @RequestMapping(value="/editsave",method = RequestMethod.POST)
    public String editsave(@ModelAttribute("std") Person std){
    	personService.update(std);
        return "redirect:/viewstd";
    }
    @RequestMapping(value="/deletestd/{id}",method = RequestMethod.GET)
    public String delete(@PathVariable int id){
    	personService.delete(id);
        return "redirect:/viewstd";
    }
}
