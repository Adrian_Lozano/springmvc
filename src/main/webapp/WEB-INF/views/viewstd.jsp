<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Added</title>
</head>
<body>
	<table>
		<tr>
			<td>Id</td>
			<td>Name</td>
			<td>Age</td>
		</tr>
		<c:forEach items="${list}" var="person">
			<tr>
				<td>${person.getId() }</td>
				<td>${person.getName() }</td>
				<td>${person.getAge() }</td>
				<td><a href="./editstd/<c:out value="${person.getId()}"/>">edit</a></td>
				<td><a href="./deletestd/<c:out value="${person.getId()}"/>">delete</a></td>
			</tr>
		</c:forEach>
	</table>
	<a href="./">Main Page</a>
</body>
</html>