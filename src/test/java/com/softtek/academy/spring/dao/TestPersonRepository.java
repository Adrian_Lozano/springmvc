package com.softtek.academy.spring.dao;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;


import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.softtek.academy.spring.beans.Person;
import com.softtek.academy.spring.configuration.PersonConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {PersonConfiguration.class})
@WebAppConfiguration
public class TestPersonRepository {
	@Autowired
	PersonRepository personRepository;
	@Test
	public void getByIdTest() {
		int actual = personRepository.getPersonById(111).getAge();
		assertNotNull(actual);
		int expected = 2020;
		assertEquals(expected, actual);
	}
}
